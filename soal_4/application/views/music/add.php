<br>
<div class="col">
    <div class="alert alert-secondary" role="alert">
        <h3>Form Add Genre</h3>
    </div>

    <form method="POST" action="<?php echo site_url(); ?>pro_music" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-6">

                <div class="form-group">
                    <label for="inpTitle">Title </label>
                    <div class="col-sm-6">

                        <input type="file" class="" id="inpTitle" name="title">
                        <!-- <p class="help-block">File dalam format .mp3!</p> -->
                    </div>

                </div>
                <div class="form-group">
                    <label for="inpDurasi">Durasi </label>
                    <input type="time" class="form-control" id="inpDurasi" name="durasi">

                </div>
                <div class="form-group">
                    <label for="inpGenre">Genre </label>
                    <select class="form-control" name="id_genre" id="inpGenre">
                        <option value="">-- Pilih Genre --</option>
                        <?php $no = 0;
                        foreach ($genre as $key => $val) : ?>
                            <option value="<?php echo $val->id; ?>">
                                <?php echo ++$no . ".  " . $val->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                </div>

                <div class="form-group">
                    <label for="inpSinger">Singer </label>
                    <select class="form-control" name="id_singer" id="inSinger">
                        <option value="">-- Pilih Singer --</option>
                        <?php $no = 0;
                        foreach ($singers as $key => $val) : ?>
                            <option value="<?php echo $val->id; ?>">
                                <?php echo ++$no . ".  " . $val->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>

            <div class="col-sm-6">

                <div class="form-group">
                    <label for="inpPhoto">Photo </label>
                    <div class="col-sm-6">

                        <input type="file" class="" id="inpPhoto" name="photo">
                    </div>

                </div>

                <div class="form-group">
                    <label for="inpDeskripsi">Deskripsi </label>
                    <textarea class="form-control" name="deskripsi" id="" cols="30" rows="6"></textarea>

                </div>
                <div class="form-group">

                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
        <div class="col">

        </div>
    </form>
</div>
<!-- </div> -->
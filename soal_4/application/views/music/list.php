<br><br>
<div class="container">
    <div class="alert alert-secondary" role="alert">
        <h3>List Music</h3>
    </div>
    <div class="col-sm-12 text-center">
        <div style="margin-top: 8px" id="message">
            <h2> <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?></h2>
        </div>
        <hr />
    </div>
    <div class="col">
        <div>

            <div class="form-group">
                <label for="inpGenre">Sortir Berdasarkan Genre </label>
                <select class="form-control" name="id_genre" id="inpGenre">
                    <option value="">-- All Genre --</option>
                    <?php $no = 0;
                    foreach ($genre as $key => $val) : ?>
                        <option value="<?php echo $val->id; ?>">
                            <?php echo ++$no . ".  " . $val->name; ?>
                        </option>
                    <?php endforeach; ?>
                </select>

            </div>
        </div>
        <div class="float-right">
            <a href="<?php echo site_url() ?>add_music" class="btn btn-primary float-right" target="_blank">Tambah</a>
        </div>

    </div>
    <br>
    <div class="col">
        <!-- <audio class="test" controls autoplay>
            //<source src="horse.ogg" type="audio/ogg">
            <source src="<?php echo site_url() . "/assets/music/Break Into The Dark.mp3"; ?>" type=" audio/mpeg"> Your browser does not support the audio element. </audio> -->
    </div>
    <br>
    <div class="row" id="content">
        <?php foreach ($dt as $key => $val) : ?>
            <div class="col-md-4">
                <div class="card mb-3">
                    <?php if (empty($val->photo)) { ?>

                        <img src="<?php echo site_url() . 'assets/music/img.jpg'; ?>" class="card-img-top item" alt="...">
                    <?php } else { ?>

                        <img src="<?php echo site_url() . 'assets/music/' . $val->photo; ?>" class="card-img-top item" alt="...">
                    <?php } ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo (!empty($val->title)) ? $val->title : 'none title'; ?></h5>
                        <p class="card-text"><?= substr($val->deskripsi, 0, 25); ?> ...</p>
                        <h5 class="card-title">Genre : <?= $val->nm_genre; ?></h5>
                        <h5 class="card-title"><?= $val->durasi; ?></h5>

                        <!-- <audio class="test" controls autoplay> -->
                        <?php if (empty($val->title)) { ?>
                            <audio class="test" controls>
                                <source src="#" type=" audio/mpeg"> Your browser does not support the audio element. </audio>
                        <?php } else { ?>
                            <audio class="test" controls>
                                <source src="<?php echo site_url() . "assets/music/" . $val->title; ?>" type=" audio/mpeg"> Your browser does not support the audio element. </audio>
                        <?php } ?>
                        <!-- <source src="<?php echo site_url() . "assets/music/Break Into The Dark.mp3"; ?>" type=" audio/mpeg"> Your browser does not support the audio element. </audio> -->
                    </div>
                    <div class="col">
                        <a href="<?php echo site_url() . 'dtl_music/' . $val->id; ?>" target="_blank">Detail</a> |
                        <a href="<?php echo site_url() . 'upd_music/' . $val->id; ?>" target="_blank">Update</a> |
                        <a href="<?php echo site_url() . 'dlt_music/' . $val->id; ?>">Delete</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>

<script>
    var url = '<?php echo site_url(); ?>';
    $(document).ready(function() {
        $("#inpGenre").on("change", function() {
            var idx = $(this).val();
            // alert('dfds');

            $.ajax({
                url: url + 'all_dt_music',
                type: "POST",
                dataType: "json",
                data: {
                    uid: idx
                },
                success: function(result) {
                    $("#content").html("");
                    console.log(result);
                    var content = "";
                    $.each(result, function(i, data) {
                        var img = ``;
                        var music = ``;

                        if (data.photo != '') {
                            img = `<img src="` + url + `assets/music/` + data.photo + `" class="card-img-top item" alt="...">`;
                        } else {
                            img = `<img src="` + url + `assets/music/img.jpg" class="card-img-top item" alt="...">`;

                        }


                        if (data.title != '') {
                            music = `<audio class="test" controls>
                            <source src="` + url + `assets/music/` + data.title + `" type=" audio/mpeg"> Your browser does not support the audio element. 
                            </audio>`;
                        } else {
                            music = `<audio class="test" controls>
                                <source src="#" type=" audio/mpeg"> Your browser does not support the audio element. </audio>`;

                        }

                        content += `
                            <div class="col-md-4">
                                <div class="card mb-3">
                                    ` + img + `
                                    <div class="card-body">
                                        <h5 class="card-title">` + data.nm_title + `</h5>
                                        <p class="card-text">` + data.deskripsi + ` ...</p>
                                        <h5 class="card-title">Genre : ` + data.nm_genre + `</h5>
                                        <h5 class="card-title">` + data.durasi + `</h5>
                                        ` + music + `
                                    </div>
                                    <div class="col">
                                    <a href="` + url + `dtl_music/` + data.id + `" target="_blank">Detail</a> |
                                    <a href="` + url + `upd_music/` + data.id + `" target="_blank">Update</a> |
                                    <a href="` + url + `dlt_music/` + data.id + `">Delete</a>
                                    </div>
                                </div>
                            </div>

                        `;
                    })
                    $("#content").html(content);

                }
            })
        });
    });
</script>
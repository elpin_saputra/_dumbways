<br>
<div class="col">
    <div class="alert alert-secondary" role="alert">
        <h3>Form Update Genre</h3>
    </div>

    <form method="POST" action="<?php echo site_url(); ?>edit_music" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-6">

                <div class="form-group">
                    <label for="inpTitle">Title </label>
                    <div class="col-sm-6">

                        <input type="file" class="" id="inpTitle" name="title">
                        <!-- <p class="help-block">File dalam format .mp3!</p> -->
                    </div>

                </div>
                <div class="form-group">
                    <label for="inpDurasi">Durasi </label>
                    <input type="time" class="form-control" id="inpDurasi" name="durasi" value="<?php echo $dt->durasi; ?>">

                </div>
                <div class="form-group">
                    <label for="inpGenre">Genre </label>
                    <select class="form-control" name="id_genre" id="inpGenre">
                        <option value="">-- Pilih Genre --</option>
                        <?php $no = 0;
                        $selG = '';
                        foreach ($genre as $key => $val) :
                            if ($dt->id_genre == $val->id)
                                $selG = 'selected';
                            else
                                $selG = '';
                        ?>
                            <option value="<?php echo $val->id; ?>" <?php echo $selG; ?>>
                                <?php echo ++$no . ".  " . $val->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                </div>

                <div class="form-group">
                    <label for="inpSinger">Singer </label>
                    <select class="form-control" name="id_singer" id="inSinger">
                        <option value="">-- Pilih Singer --</option>
                        <?php $no = 0;
                        $selS = '';
                        foreach ($singers as $key => $val) :
                            if ($dt->id_singer == $val->id)
                                $selS = 'selected';
                            else
                                $selS = '';
                        ?>
                            <option value="<?php echo $val->id; ?>" <?php echo $selS; ?>>
                                <?php echo ++$no . ".  " . $val->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>

            <div class="col-sm-6">

                <div class="form-group">
                    <label for="inpPhoto">Photo </label>
                    <div class="col-sm-6">

                        <input type="file" class="" id="inpPhoto" name="photo">
                    </div>

                </div>

                <div class="form-group">
                    <label for="inpDeskripsi">Deskripsi </label>
                    <textarea class="form-control" name="deskripsi" id="" cols="30" rows="6"><?php echo $dt->deskripsi; ?></textarea>

                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="<?php echo $dt->id; ?>">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
        <div class="col">

        </div>
    </form>
</div>
<!-- </div> -->
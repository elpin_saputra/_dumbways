<br>
<div class="col">
    <div class="alert alert-secondary" role="alert">
        <h3>Form Update Genre</h3>
    </div>

    <form method="POST" action="<?php echo site_url(); ?>edit_genre">
        <div class="form-group">
            <label for="inpName">Name </label>
            <input type="text" class="form-control" id="inpName" value="<?php echo $dt->name; ?>" name="name" required>

        </div>
        <input type="hidden" name="id" value="<?php echo $dt->id; ?>">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<!-- </div> -->
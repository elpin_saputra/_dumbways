<br>
<div class="col">
    <div class="alert alert-secondary" role="alert">
        <h3>Form Add Genre</h3>
    </div>

    <form method="POST" action="<?php echo site_url(); ?>pro_genre">
        <div class="form-group">
            <label for="inpName">Name </label>
            <input type="text" class="form-control" id="inpName" name="name" required>

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<!-- </div> -->
<br><br>
<div class="container">
    <div class="alert alert-secondary" role="alert">
        <h3>List Singers</h3>
    </div>
    <div class="col-sm-12 text-center">
        <div style="margin-top: 8px" id="message">
            <h2> <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?></h2>
        </div>
        <hr />
    </div>
    <div class="col">

        <div class="float-right">
            <a href="<?php echo site_url() ?>add_singers" class="btn btn-primary float-right" target="_blank">Tambah</a>
        </div>
    </div>
    <br>
    <div class="col">

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 5%" scope="col">#</th>
                    <th scope="col">Name</th>
                    <th style="width: 25%; " scope="col">Action</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $no = 0;
                foreach ($dt as $key => $val) :

                ?>
                    <tr>
                        <td><?php echo ++$no; ?></td>
                        <td><?php echo $val->name; ?></td>
                        <td style="text-align:center">
                            <a href="<?php echo site_url() . 'dtl_singers/' . $val->id; ?>" target="_blank">Detail</a> |
                            <a href="<?php echo site_url() . 'upd_singers/' . $val->id; ?>" target="_blank">Update</a> |
                            <a href="<?php echo site_url() . 'dlt_singers/' . $val->id; ?>">Delete</a>
                        </td>
                    </tr>


                <?php endforeach; ?>


            </tbody>
        </table>
    </div>
</div>
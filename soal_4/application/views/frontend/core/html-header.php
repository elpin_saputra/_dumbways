<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $pageTitle; ?> Elpin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        table {
            table-layout: fixed;
            width: 100%;
        }

        .item {
            width: 120px;
            height: 120px;
            height: auto;
            float: left;
            margin: 3px;
            padding: 3px;
        }
    </style>

</head>

<body>
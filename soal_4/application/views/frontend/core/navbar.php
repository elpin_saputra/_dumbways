<div>

    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #57B846;">
        <a class="navbar-brand" href="<?php echo site_url(); ?>">MUSIC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!-- <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url(); ?>">Home</span></a>
                </li> -->
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Master Data
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_genre">List Genre</a>

                    </div>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_genre">List Genre</a>
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_singers">List Singers</a>
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_music">List Music</a>
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_music_by_genre">List Music By Genre</a>

                    </div>
                </li> -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Master Data
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_genre">List Genre</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_singers">List Singers</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo site_url(); ?>list_music">List Music</a>
                        <div class="dropdown-divider"></div>
                        <!-- <a class="dropdown-item" href="<?php echo site_url(); ?>list_music_by_genre">List Music By Genre</a> -->

                    </div>
                </li>



            </ul>

        </div>
    </nav>
</div>
<div class="container">
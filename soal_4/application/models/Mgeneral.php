<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mgeneral extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    #Get All Record
	#ex akses : $this->mgeneral->getAll('nama_tabel');
	function GetAll($tabel, $result = 'object',$order_field="", $order_tipe=""){
		if($order_field!="" && $order_tipe!=""){
			$this->db->order_by($order_field,$order_tipe);
		}
		if($result =='object'){
			$return = 	$this->db->get($tabel)->result();
		}else{
			$return = 	$this->db->get($tabel)->result_array();

		}
		return $return;
	}

	#Get Where Record
	#ex akses : $this->mgeneral->getWhere(array('field1'=>'data','field2'=>'data'),'nama_tabel');
	function GetWhere($where,$tabel,$order_field="",$order_tipe=""){
		$this->db->where($where);
		if($order_field!=""){ $this->db->order_by($order_field,$order_tipe); }
		return $this->db->get($tabel)->result();
	}

	#Get Where one row Record
	#ex akses : $this->mgeneral->GetRow(array('field1'=>'data','field2'=>'data'),'nama_tabel');
	function GetRow($where,$tabel,$order_field="",$order_tipe=""){
		$this->db->where($where);
		if($order_field!=""){ $this->db->order_by($order_field,$order_tipe); }
		return $this->db->get($tabel)->row();
	}

	#Get Like Record
	#ex akses : $this->mgeneral->getLike(array('field1'=>'data','field2'=>'data'),'nama_tabel');
	function GetLike($where,$tabel){
		$this->db->or_like($where);
		return $this->db->get($tabel)->result();
	}

	#Insert Record
	#ex akses : $this->mgeneral->Save(array_data_insert,nama_tabel);
	function Save($varData,$tabel){
		$this->db->insert($tabel, $varData);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}

	#Update Record
	#ex akses : $this->mgeneral->Update(array('field1'=>'data','field2'=>'data'),array_data_update,nama_tabel);
	function Update($where, $data, $tabel) {
		$this->db->where($where);
		$this->db->update($tabel, $data);
    //echo $this->db->last_query();
		return $this->db->trans_status(); //return FALSE or TRUE
	}

	#fungsi hapus data
	#ex akses : $this->mgeneral->delete(array('field1'=>'data','field2'=>'data'),nama_tabel);
	function Delete($where,$tabel){
		$this->db->where($where);
		$this->db->delete($tabel);

		return $this->db->trans_status(); //returcodecoden FALSE or TRUE
	}
	function prosedure($fn,$param =  array()){
		$param =  "('".implode("','",$param)."');";
		$this->db->query("call ".$fn.$param);
		return true;
	}

	function procedure($fn,$param =  array()){
		$param =  "('".implode("','",$param)."');";
		return $this->db->query("call ".$fn.$param);
	}
}

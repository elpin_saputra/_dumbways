<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'music';
$route['404_override'] = 'Not_found/index';
$route['translate_uri_dashes'] = FALSE;

$route['list_genre'] = 'genre';
$route['add_genre'] = 'genre/add';
$route['pro_genre'] = 'genre/pro';
$route['edit_genre'] = 'genre/edit';
$route['dtl_genre/(:any)'] = 'genre/dtl/$1';
$route['upd_genre/(:any)'] = 'genre/upd/$1';
$route['dlt_genre/(:any)'] = 'genre/dlt/$1';


$route['list_singers'] = 'singers';
$route['add_singers'] = 'singers/add';
$route['pro_singers'] = 'singers/pro';
$route['edit_singers'] = 'singers/edit';
$route['dtl_singers/(:any)'] = 'singers/dtl/$1';
$route['upd_singers/(:any)'] = 'singers/upd/$1';
$route['dlt_singers/(:any)'] = 'singers/dlt/$1';

$route['list_music'] = 'music';
$route['all_dt_music'] = 'music/all_dt';
$route['add_music'] = 'music/add';
$route['pro_music'] = 'music/pro';
$route['edit_music'] = 'music/edit';
$route['dtl_music/(:any)'] = 'music/dtl/$1';
$route['upd_music/(:any)'] = 'music/upd/$1';
$route['dlt_music/(:any)'] = 'music/dlt/$1';

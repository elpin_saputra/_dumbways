<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Genre extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $title = "genre";
        $pages = "genre/list";

        $all = $this->Mgeneral->GetAll('genre');
        $data = [
            'dt' => $all,
        ];

        $this->layout($title, $pages, $data);
    }

    public function add()
    {
        $title = "add genre ";
        $pages = "genre/add";
        $data = "";

        $this->layout($title, $pages, $data);
    }

    public function pro()
    {
        $data = $this->input->post();

        // var_dump($data);
        $this->Mgeneral->save($data, 'genre');

        $this->session->set_flashdata('message', 'Data successfully saved.');
        redirect(site_url('list_genre'));
    }

    public function dtl($id = '')
    {
        $title = 'detail genre';
        $pages = 'genre/dtl';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'genre');

        $data = [
            'dt' => $get,
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function upd($id = '')
    {
        $title = 'update genre';
        $pages = 'genre/upd';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'genre');

        $data = [
            'dt' => $get,
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function edit()
    {
        $data = $this->input->post();

        // var_dump($data);
        $idx = $data['id'];
        unset($data['id']);
        $this->Mgeneral->update(['id' => $idx], $data, 'genre');

        $this->session->set_flashdata('message', 'Data successfully updated.');
        redirect(site_url('list_genre'));
    }

    public function dlt($id = '')
    {
        $this->Mgeneral->delete(['id' => $id], 'genre');


        $this->session->set_flashdata('message', 'Data successfully deleted.');
        redirect(site_url('list_genre'));
    }
}

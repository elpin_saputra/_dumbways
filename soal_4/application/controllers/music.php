<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Music extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $title = "music";
        $pages = "music/list";

        $all = $this->Mgeneral->GetAll('music');
        foreach ($all as $key => $val) :
            $gnr = $this->Mgeneral->GetRow(['id' => $val->id_genre], 'genre');
            $sngr = $this->Mgeneral->GetRow(['id' => $val->id_singer], 'singers');

            $val->nm_genre = (!empty($gnr->name)) ? $gnr->name : 'none genre';
            $val->nm_singer = (!empty($sngr->name)) ? $sngr->name : 'none singer';

        endforeach;
        $data = [
            'dt' => $all,
            'genre' => $this->Mgeneral->GetAll('genre'),
        ];

        $this->layout($title, $pages, $data);
    }

    public function all_dt()
    {
        $idx = $this->input->post('uid');

        if (!empty($idx))
            $all = $this->Mgeneral->GetWhere(['id_genre' => $idx], 'music');
        else
            $all = $this->Mgeneral->GetAll('music');

        foreach ($all as $key => $val) :
            $gnr = $this->Mgeneral->GetRow(['id' => $val->id_genre], 'genre');
            $sngr = $this->Mgeneral->GetRow(['id' => $val->id_singer], 'singers');
            $val->nm_title = (!empty($val->title)) ? $val->title : 'none title';
            $val->nm_genre = (!empty($gnr->name)) ? $gnr->name : 'none genre';
            $val->nm_singer = (!empty($sngr->name)) ? $sngr->name : 'none singer';

        endforeach;

        echo json_encode($all);
    }

    public function add()
    {
        $title = "add music ";
        $pages = "music/add";
        $data = [
            'genre' => $this->Mgeneral->GetAll('genre'),
            'singers' => $this->Mgeneral->GetAll('singers'),

        ];

        $this->layout($title, $pages, $data);
    }

    public function pro()
    {
        $data = $this->input->post();

        // unset($data['durasi']);

        // var_dump($data);
        // die();
        $config['upload_path'] = "./assets/music";
        $config['allowed_types'] = 'mp3|jpeg|jpg|png';
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);


        $fileMusic = "";
        $filePhoto = "";

        if ($this->upload->do_upload('title')) {
            $fileMusic = $this->upload->data();

            $data['title'] = $fileMusic['file_name'];
        }
        if ($this->upload->do_upload('photo')) {
            $filePhoto = $this->upload->data();

            $data['photo'] = $filePhoto['file_name'];
        }

        $this->Mgeneral->save($data, 'music');

        $this->session->set_flashdata('message', 'Data successfully saved.');
        redirect(site_url('list_music'));
    }

    public function dtl($id = '')
    {
        $title = 'detail music';
        $pages = 'music/dtl';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'music');
        $gnr = $this->Mgeneral->GetRow(['id' => $get->id_genre], 'genre');
        $sngr = $this->Mgeneral->GetRow(['id' => $get->id_singer], 'singers');

        $data = [
            'dt' => $get,
            'gnr' => $gnr,
            'sngr' => $sngr,
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function upd($id = '')
    {
        $title = 'update music';
        $pages = 'music/upd';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'music');

        $data = [
            'dt' => $get,
            'genre' => $this->Mgeneral->GetAll('genre'),
            'singers' => $this->Mgeneral->GetAll('singers'),
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function edit()
    {
        $data = $this->input->post();

        // unset($data['durasi']);

        // var_dump($data);
        // die();
        $title = $this->input->post('title');
        $photo = $this->input->post('photo');

        $config['upload_path'] = "./assets/music";
        $config['allowed_types'] = 'mp3|jpeg|jpg|png';
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);


        $fileMusic = "";
        $filePhoto = "";

        if ($this->upload->do_upload('title')) {
            $fileMusic = $this->upload->data();
            if (!empty($title))
                $data['title'] = $fileMusic['file_name'];
        }
        if ($this->upload->do_upload('photo')) {
            $filePhoto = $this->upload->data();

            if (!empty($photo))
                $data['photo'] = $filePhoto['file_name'];
        }

        if (empty($title))
            unset($data['title']);


        if (empty($photo))
            unset($data['photo']);



        // var_dump($data);
        $idx = $data['id'];
        unset($data['id']);
        $this->Mgeneral->update(['id' => $idx], $data, 'music');

        $this->session->set_flashdata('message', 'Data successfully updated.');
        redirect(site_url('list_music'));
    }

    public function dlt($id = '')
    {
        $this->Mgeneral->delete(['id' => $id], 'music');


        $this->session->set_flashdata('message', 'Data successfully deleted.');
        redirect(site_url('list_music'));
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Singers extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $title = "singers";
        $pages = "singers/list";

        $all = $this->Mgeneral->GetAll('singers');
        $data = [
            'dt' => $all,
        ];

        $this->layout($title, $pages, $data);
    }

    public function add()
    {
        $title = "add singers ";
        $pages = "singers/add";
        $data = "";

        $this->layout($title, $pages, $data);
    }

    public function pro()
    {
        $data = $this->input->post();

        // var_dump($data);
        $this->Mgeneral->save($data, 'singers');

        $this->session->set_flashdata('message', 'Data successfully saved.');
        redirect(site_url('list_singers'));
    }

    public function dtl($id = '')
    {
        $title = 'detail singers';
        $pages = 'singers/dtl';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'singers');

        $data = [
            'dt' => $get,
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function upd($id = '')
    {
        $title = 'update singers';
        $pages = 'singers/upd';
        $get = $this->Mgeneral->GetRow(['id' => $id], 'singers');

        $data = [
            'dt' => $get,
        ];

        // print_r($data);
        // die();

        $this->layout($title, $pages, $data);
    }

    public function edit()
    {
        $data = $this->input->post();

        // var_dump($data);
        $idx = $data['id'];
        unset($data['id']);
        $this->Mgeneral->update(['id' => $idx], $data, 'singers');

        $this->session->set_flashdata('message', 'Data successfully updated.');
        redirect(site_url('list_singers'));
    }

    public function dlt($id = '')
    {
        $this->Mgeneral->delete(['id' => $id], 'singers');


        $this->session->set_flashdata('message', 'Data successfully deleted.');
        redirect(site_url('list_singers'));
    }
}

<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller
{

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * CI_Loader
	 *
	 * @var	CI_Loader
	 */
	public $load;
	protected $sesi_log = "", $group = "", $sesi_id = "";

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance = &$this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class) {
			$this->$var = &load_class($class);
		}

		$this->load = &load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
		date_default_timezone_set('Asia/Jakarta');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	public function header_login($title = "")
	{
		$dataHeader = array(
			"pageTitle" => $title,

		);


		return $this->load->view('frontend/login/html-header', $dataHeader);
	}

	public function footer_login()
	{

		$dataFooter = array();

		return $this->load->view('frontend/login/html-footer', $dataFooter);
	}

	public function layout_login($title = "", $pages = "", $data = "")
	{

		$pageTitle = $title;
		$pages = $pages;
		$comp = array(
			'header' => $this->header_login($pageTitle),
			'content' => $this->load->view($pages, $data),
			'footer' => $this->footer_login(),

		);


		return $this->load->view('frontend/pages/index', $comp);
	}

	public function header($title = "")
	{
		$dataHeader = array(
			"pageTitle" => $title,

		);

		return $this->load->view('frontend/core/html-header', $dataHeader);
	}


	public function navbar()
	{

		$dataNavbar = array();

		// if ($this->aauth->is_loggedin()) {
		// 	$member_user = true;
		// 	$dataNavbar['group'] = 1;
		// 	// $dataNavbar['group'] = $this->group->group_id;
		// 	$dataNavbar['status_auth'] = $member_user;

		// 	$group = $this->aauth->get_user_groups($this->sesi_log->id);

		// } else {
		// 	$member_user = false;
		// 	$dataNavbar['group'] = "";
		// 	$dataNavbar['status_auth'] = $member_user;
		// }

		return $this->load->view('frontend/core/navbar', $dataNavbar);
	}

	public function footer()
	{
		$dataFooter = array();

		return $this->load->view('frontend/core/html-footer', $dataFooter);
	}
	public function layout($title = "", $pages = "", $data = "")
	{
		// if ($this->aauth->is_loggedin()) {
		// 	$data['sesiLog'] = $this->sesi_log;
		// 	$data['sesiID'] = $this->sesi_log->id;
		// } else {
		// 	$data['sesiLog'] = "";
		// 	$data['sesiID'] = "";
		// }

		$comp = array(
			'header' => $this->header($title),
			'navbar' => $this->navbar(),
			'content' => $this->load->view($pages, $data),
			'footer' => $this->footer(),

		);

		return $this->load->view('frontend/pages/index', $comp);
	}
}
